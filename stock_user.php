<?php

	session_start();
	if (!isset($_SESSION['user_login_status']) AND $_SESSION['user_login_status'] != 2) {
        header("location: login.php");
		exit;
        }

	/* Connect To Database*/
	require_once ("config/db.php");//Contiene las variables de configuracion para conectar a la base de datos
	require_once ("config/conexion.php");//Contiene funcion que conecta a la base de datos
	
	$active_productos="active";
	$title="SKYTACTIC | Visor de Mapas";
?>
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <head>
    <?php include("head_user.php");?>
  </head>
  <body>
	<?php
	include("navbar_user.php");
	?>
	
			<div class="index-content">
				<div class="container">
					
					    	    <h1 class="text-center"><span>SKYTACTIC - Inspección de Obra</span></h1>
					    	    
										<a href="blog-ici.html">
											<div class="col-lg-4">
												<div class="card">
													<img src="http://skytactic.com.mx/visor2d/img/inspeccion.jpg">
													<h4>Inspección de Obra - Puente "La Resurección" </h4>
													<p>Seguimiento de obra del puente "La Resurección"</p>
													<p>Puebla, México</p>
												</div>
											</div>
										</a>


				</div>
			</div>

	
	<?php
	include("footer.php");
	?>
  </body>
</html>
