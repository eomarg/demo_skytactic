<?php

	session_start();
	if (!isset($_SESSION['user_login_status']) AND $_SESSION['user_login_status'] != 3) {
        header("location: login.php");
		exit;
        }

	/* Connect To Database*/
	require_once ("config/db.php");//Contiene las variables de configuracion para conectar a la base de datos
	require_once ("config/conexion.php");//Contiene funcion que conecta a la base de datos
	
	$active_productos="active";
	$title="SKYTACTIC | Visor de Mapas";
?>
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <head>
    <?php include("head_user.php");?>
  </head>
  <body>
	<?php
	include("navbar_user.php");
	?>
	
			<div class="index-content">
				<div class="container">
					
					    	    <h1 class="text-center"><span>SKYTACTIC - LISTADO DE PROYECTOS</span></h1>
					    	    
										<a href="http://skytactic.com.mx/demo/cli/cantera/cantera_index.php">
											<div class="col-lg-4 text-center">
												<div class="card">
													<img src="http://skytactic.com.mx/visor2d/img/mineria.jpg">
													<h4>Ejemplo - "Cantera" </h4>
													<p> </p>
													<p> </p>
												</div>
											</div>
										</a>
										<a href="http://skytactic.com.mx/demo/cli/minera/minera_index.php">
											<div class="col-lg-4 text-center">
												<div class="card">
													<img src="http://skytactic.com.mx/visor2d/img/mineria.jpg">
													<h4>Ejemplo - "Minería" </h4>
													<p> </p>
													<p> </p>
												</div>
										    </div>
										</a>
										<a href="http://skytactic.com.mx/demo/cli/agricultura/agro_index.php">
											<div class="col-lg-4 text-center">
												<div class="card">
													<img src="http://skytactic.com.mx/visor2d/img/agricultura.jpg">
													<h4>Ejemplo - "Agro" </h4>
													<p> </p>
													<p> </p>
												</div>
											</div>
										</a>
										<a href="http://skytactic.com.mx/demo/cli/inspeccion/inspeccion_index.php">
											<div class="col-lg-4 text-center">
												<div class="card">
													<img src="http://skytactic.com.mx/visor2d/img/inspeccion.jpg">
													<h4>Ejemplo - "Inspección" </h4>
													<p> </p>
													<p> </p>
												</div>
											</div>
										</a>										


				</div>
			</div>

	
	<?php
	include("footer.php");
	?>
  </body>
</html>
