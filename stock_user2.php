<?php

	session_start();
	if (!isset($_SESSION['user_login_status']) AND $_SESSION['user_login_status'] != 1) {
        header("location: login.php");
		exit;
        }

	/* Connect To Database*/
	require_once ("config/db.php");//Contiene las variables de configuracion para conectar a la base de datos
	require_once ("config/conexion.php");//Contiene funcion que conecta a la base de datos
	
	$active_productos="active";
	$title="SKYTACTIC | Visor de Mapas";
?>
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <head>
    <?php include("head_user.php");?>
  </head>
  <body>
	<?php
	include("navbar_user.php");
	?>
	
							 <div class="container-fluid">
										<div class="row">
													<div class="col-md-12">
																<table class="table table-hover">
																	  <thead>
																		<tr>
																		  <th scope="col">#</th>
																		  <th scope="col">First</th>
																		  <th scope="col">Last</th>
																		  <th scope="col">Handle</th>
																		</tr>
																	  </thead>
																	  <tbody>
																		<tr>
																		  <th scope="row">1</th>
																		  <td>Mark</td>
																		  <td>Otto</td>
																		  <td>@mdo</td>
																		</tr>
																		<tr>
																		  <th scope="row">2</th>
																		  <td>Jacob</td>
																		  <td>Thornton</td>
																		  <td>@fat</td>
																		</tr>
																		<tr>
																		  <th scope="row">3</th>
																		  <td colspan="2">Larry the Bird</td>
																		  <td>@twitter</td>
																		</tr>
																	  </tbody>
																	</table>
													</div>

										</div>
								</div>

	
	<?php
	include("footer.php");
	?>
  </body>
</html>
