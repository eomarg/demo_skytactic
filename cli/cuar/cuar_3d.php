<?php

	session_start();
	if (!isset($_SESSION['user_login_status']) AND $_SESSION['user_login_status'] != 3) {
        header("location: login.php");
		exit;
        }

	/* Connect To Database*/
	require_once ("../../config/db.php");//Contiene las variables de configuracion para conectar a la base de datos
	require_once ("../../config/conexion.php");//Contiene funcion que conecta a la base de datos
	
	$active_productos="active";
	$title="SKYTACTIC | Visor de Mapas";
?>
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <style type='text/css'>
	 
.mainContainer {
  min-height: 100%;
  height: 100%;
  margin: 0 auto -40px;

		
}
	 
.full-height {
  height: 100%;
  min-height: 100%;
}
	 
	 
	</style>
  <head>
    <?php include("../../head_user.php");?>
  </head>
  <body>
	<?php
	include("../../navbar_user.php");
	?>
	

				<div class="container-fluid mainContainer">
					<div class="row full-height">
        						<div class="col-md-12 full-height" >

        							<iframe width="100%" height="100%" src="http://skytactic.com.mx/visor3d/cuar/CUAR.html" frameborder="0" style="border:0" allowfullscreen scrolling="no"></iframe>
        						</div>
					</div>
				</div>


	

  </body>
</html>
