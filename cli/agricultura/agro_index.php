<?php

	session_start();
	if (!isset($_SESSION['user_login_status']) AND $_SESSION['user_login_status'] != 3) {
        header("location: login.php");
		exit;
        }

	/* Connect To Database*/
	require_once ("../../config/db.php");//Contiene las variables de configuracion para conectar a la base de datos
	require_once ("../../config/conexion.php");//Contiene funcion que conecta a la base de datos

	$active_productos="active";
	$title="SKYTACTIC | Visor de Mapas";
?>
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <head>
    <?php include("../../head_user.php");?>
    <link rel="stylesheet" type="text/css" href="../../css/demo.css" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body>
	<?php
	include("../../navbar_user.php");
	?>


				<div class="container-fluid">
						<div id="sidebar" style="display: inline;">
							<div class="card card bg-light mb-3 text-center">
							  <h2 class="card-header">Módulo de Información</h2>
							  <div class="card-body">
										<table class="table table-hover" id="tabla1">
											     <thead class="thead-dark">
												   <tr>
														<th>Cultivo</th>
														<th>Número de Arboles</th>
													   <th>Altura promedio</th>
														<th>Etapa fenologica</th>
														<th>% Área afectada</th>
												   </tr>
												 </thead>
													<tr>
													<td>Limón Persa</td>
													<td>349</td>
													<td>1.71 m</td>
													  <td>Desarrollo del fruto</td>
													  <td>37%</td>
												   </tr>
												<tr>
													<td>Limón Mexicano</td>
													<td>300</td>
													<td>2.10 m</td>
													  <td>Desarrollo del fruto</td>
													  <td>29%</td>
												</tr>
												</table>
							  </div>
							    <div class="card-footer text-muted">
									16 de Febrero 2018
								  </div>
									<form method="get" action="assets/reporte_agricultura.pdf">
										<button class="btn" type="submit"><i class="fa fa-download"></i> Descargar reporte </button>
									</form>
							</div>


						</div>
						<div id="map" style="display: inline;"><iframe frameborder="0" src="http://skytactic.com.mx/visor2d/limon/visor.html" style="border: 0px; vertical-align: bottom;"></iframe></div>
				</div>




  </body>
</html>
