<?php

	session_start();
	if (!isset($_SESSION['user_login_status']) AND $_SESSION['user_login_status'] != 3) {
        header("location: login.php");
		exit;
        }

	/* Connect To Database*/
	require_once ("../../config/db.php");//Contiene las variables de configuracion para conectar a la base de datos
	require_once ("../../config/conexion.php");//Contiene funcion que conecta a la base de datos
	
	$active_productos="active";
	$title="SKYTACTIC | CALIDRA 3D";
?>
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <head>
  <title><?php echo $title;?></title>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script> 
   <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
   <link rel="stylesheet" href="../../css/menu_opciones.css" />
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

	
	<link rel="stylesheet" href="../../css/custom.css">
	<link rel=icon href='../../img/favicon.ico' sizes="32x32" type="image/png">
  </head>
  <body>
	<?php
	include("../../navbar_user.php");
	?>
	
							 <div class="container-fluid">
								<div class="row">
									<div class="embed-container">
																				<iframe src="http://skytactic.com.mx/visor3d/calidra/Calidra.html" frameborder="0" style="border:0" allowfullscreen scrolling="no"></iframe>
									</div>
								<!--	<div class="col-md-6">
																				<iframe width="100%" height="1000" src="https://datastudio.google.com/embed/reporting/0B5FF6JBKbNJxOWItcWo2SVVVeGc/page/DjD" frameborder="0" style="border:0" allowfullscreen scrolling="no"></iframe>
									</div>
										<div class="col-md-6">
											<img src="img/leyendandvi.PNG" width="185" height="157">
											<img src="img/leyendadem.PNG" width="185" height="157"> 
										</div> -->
										
									</div>
								</div>

	

  </body>
</html>
