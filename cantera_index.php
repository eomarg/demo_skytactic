<?php

	session_start();
	if (!isset($_SESSION['user_login_status']) AND $_SESSION['user_login_status'] != 3) {
        header("location: login.php");
		exit;
        }

	/* Connect To Database*/
	require_once ("config/db.php");//Contiene las variables de configuracion para conectar a la base de datos
	require_once ("config/conexion.php");//Contiene funcion que conecta a la base de datos
	
	$active_productos="active";
	$title="SKYTACTIC | Visor de Mapas";
?>
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <head>
    <?php include("head_user.php");?>
  </head>
  <body>
	<?php
	include("navbar_user.php");
	?>
	

				<div class="container-fluid">
					<div class="row">
						        <div class="col-md-1">
									<div class="btn-group-vertical">
										<a href="http://skytactic.com.mx/demo/cli/cantera/cantera3d.php">
									  		  <button type="button" class="btn btn-info">Mapa 3D</button>
									  		  <button type="button" class="btn btn-info">Mapa 2D</button>
										</a>									  
									</div>
						        </div>
        						<div class="col-md-11">

        							<iframe width="100%" height="700" src="http://skytactic.com.mx/visor2d/limon/visor.html" frameborder="0" style="border:0" allowfullscreen scrolling="no"></iframe>
        						</div>
					</div>
				</div>


	
	<?php
	include("footer.php");
	?>
  </body>
</html>
